# YFinanceStockPrice

Allows you to asynchronously get the current stock price of a business symbol from Yahoo Finance. Effectively a partial Swift translation of the Python `yahoo_fin` library. 

## Usage
See [https://gitlab.com/Meorge/YFinanceStockPriceTesterSwift/-/blob/main/YFinanceTester/AppDelegate.swift](https://gitlab.com/Meorge/YFinanceStockPriceTesterSwift/-/blob/main/YFinanceTester/AppDelegate.swift) for example usage
