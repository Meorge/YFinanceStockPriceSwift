import XCTest

import YFinanceStockPriceTests

var tests = [XCTestCaseEntry]()
tests += YFinanceStockPriceTests.allTests()
XCTMain(tests)
