import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(YFinanceStockPriceTests.allTests),
    ]
}
#endif
