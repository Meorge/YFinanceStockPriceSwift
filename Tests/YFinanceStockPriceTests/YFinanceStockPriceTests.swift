import XCTest
@testable import YFinanceStockPrice

final class YFinanceStockPriceTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(YFinanceStockPrice().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
