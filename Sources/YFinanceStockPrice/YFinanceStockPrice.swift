import Foundation

let base_url = "https://query1.finance.yahoo.com/v8/finance/chart/"


public class YFinanceStockSymbol {
    var symbol: String
    
    var financeInfo: YFinanceFormat?
    
    var onDataReceived: (YFinanceFormat) -> Void
    
    public init(_ sym: String, onReceive: @escaping (YFinanceFormat) -> Void) {
        symbol = sym
        onDataReceived = onReceive
    }
    
    public func currentMarketPrice() -> Double {
        if financeInfo != nil {
            return financeInfo!.chart.result[0].meta.regularMarketPrice
        } else {
            return 0.0
        }
    }
    
    public func fetch() {
        YFinanceStockPriceUtilities.get_data(for: symbol, onSuccess: self.parseJSON, onError: self.handleError)
    }
    
    func parseJSON(json: String, respCode: URLResponse) {
        do {
            let stuff = try JSONDecoder().decode(YFinanceFormat.self, from: Data(json.utf8))
            self.financeInfo = stuff
            onDataReceived(self.financeInfo!)
        } catch {
            print("Error deserializing json: \(error) - \(error.localizedDescription)")
        }
    }
    
    func handleError(_ error: Error) {
        print("error :( - \(error)")
    }
}


public class YFinanceStockPriceUtilities {
    static func build_url(for ticker: String, startingAt start_date: Date?, endingAt end_date: Date?, withInterval interval: String = "1d") -> String {
        let start_seconds = (start_date == nil) ? 7223400 : Int(start_date!.timeIntervalSince1970)
        let end_seconds = (end_date == nil) ? Int(Date().timeIntervalSince1970) : Int(end_date!.timeIntervalSince1970)
        
        let params: [String: Any] = ["period1": start_seconds, "period2": end_seconds, "interval": interval.lowercased(), "events": "div,splits"]
        
        let site = base_url + ticker + params_to_string(params)
    
        return site
    }
    
    static func params_to_string(_ params: [String: Any]) -> String {
        var stringOut = "?"
        
        for (key, value) in params {
            stringOut += "\(key)=\(value)&"
        }
        
        // remove the last ampersand
        stringOut.removeLast()
        
        return stringOut
    }
    
    public static func get_data(for ticker: String, startingAt start_date: Date? = nil, endingAt end_date: Date? = nil, withIndexAsDate: Bool = true, withInterval interval: String = "1d", onSuccess: @escaping (String, URLResponse) -> Void, onError: @escaping (Error) -> Void) {
        
        let url = build_url(for: ticker, startingAt: start_date, endingAt: end_date)
        
        print("url=\(url)")
        let get_req = GETRequest(forURL: url, onComplete: onSuccess, onError: onError)
        
        get_req.makeRequest()
        
    }
}


class GETRequest {
    var url: URL?
    private var urlRequest: URLRequest?
    
    private var onCompleteFunc: (String, URLResponse) -> Void
    private var onErrorFunc: (Error) -> Void
    
    init(forURL urlString: String, onComplete: @escaping (String, URLResponse) -> Void, onError: @escaping (Error) -> Void) {
        url = URL(string: urlString)
        
        urlRequest = URLRequest(url: url!)
        
        urlRequest?.httpMethod = "GET"
        
        onCompleteFunc = onComplete
        onErrorFunc = onError
        

    }
    
    func makeRequest() {
        let task = URLSession.shared.dataTask(with: urlRequest!) { (data, response, error) in
            
            // an error happened
            if let error = error {
                print("error!")
                self.onErrorFunc(error)
                return
            }
            
            // read status code
            if let response = response as? HTTPURLResponse {
            } else {
                self.onErrorFunc(ResponseCodeNotCreated())
                return
            }
            
            // convert data to string
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("data converted!")
                self.onCompleteFunc(dataString, response!)
                return
            }
        }
        
        task.resume()
    }
}

struct ResponseCodeNotCreated: Error {
}
