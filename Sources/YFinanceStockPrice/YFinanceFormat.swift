//
//  File.swift
//  
//
//  Created by Malcolm Anderson on 8/5/20.
//

import Foundation

/*
 Note: There's a *LOT* more data in this JSON format than I've parsed here,
 but since the person only wanted to get the current market price, I'm
 trying to simplify it

*/
public class YFinanceFormat: Decodable {
    var chart: YFinanceChart
    
    public func currentMarketPrice() -> Double {
        return chart.result[0].meta.regularMarketPrice
    }
}

public class YFinanceChart: Decodable {
    var result: [YFinanceResult]
    
    private enum CodingKeys: String, CodingKey {
        case result
    }
}

public class YFinanceResult: Decodable {
    var meta: YFinanceResultMeta
    
    private enum CodingKeys: String, CodingKey {
        case meta
    }
}

public class YFinanceResultMeta: Decodable {
    var currency: String
    var regularMarketPrice: Double
    
    private enum CodingKeys: String, CodingKey {
        case currency, regularMarketPrice
    }
}
